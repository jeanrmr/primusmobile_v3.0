import { StyleSheet } from 'react-native'
import colors from './colors'
import sizes from './sizes'

export default StyleSheet.create({
  text: {
    fontSize: 15,
    color: colors.defaultText
  },
  subTitle: {
    fontSize: 18,
    textAlign: 'justify'
  },
  title: {},
  formTextInputError: {
    borderColor: colors.danger,
    borderWidth: 0.5
  }
})

export { sizes, colors }
