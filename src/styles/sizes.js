import { Platform, Dimensions } from 'react-native'

const { width, height } = Dimensions.get('window')

export default {
  normal: 8,
  title: 14,
  subTitle: 12,
  headerMarginBottom: 12,
  blur: Platform.OS === 'ios' ? 6.0 : 2.5,
  dimensions: { width, height }
}
