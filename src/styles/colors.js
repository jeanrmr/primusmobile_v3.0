export default {
  backgroundDefault: '#202D39',
  highBackground: '#10171D',
  hightLightText: '#BD833F',
  defaultText: '#fff',
  secondaryText: '#aaa',
  label: '#fff',
  backgroundCard: '#34414d',
  white: '#fff',
  black: '#000',
  danger: 'red',

  whiteTransparent: 'rgba(255, 255, 255, 0.4)',
  blackTransparent: 'rgba(0, 0, 0, 0.5)'
}
