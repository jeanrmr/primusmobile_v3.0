import React, { Component } from 'react'
import { StyleSheet, View } from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as authDuck from '~/store/ducks/auth'
import colors from '~/styles/colors'

class AuthLoadingScreen extends Component {
  static navigationOptions = {
    header: null
  }

  componentDidMount() {
    const { verifyUserInfo, navigation } = this.props
    verifyUserInfo(navigation)
  }

  render() {
    return (
      <View style={style.parent}>
        <View style={style.container} />
      </View>
    )
  }
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  parent: {
    flex: 1,
    backgroundColor: colors.backgroundDefault
  }
})

const mapDispatchToProps = dispatch => bindActionCreators({ verifyUserInfo: authDuck.verifyUserInfo }, dispatch)

export default connect(
  null,
  mapDispatchToProps
)(AuthLoadingScreen)
