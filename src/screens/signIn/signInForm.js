import React from 'react'
import { TextInput, PixelRatio, KeyboardAvoidingView } from 'react-native'
import { View } from 'native-base'
import { RkButton } from 'react-native-ui-kitten'
import { withFormik } from 'formik'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Creators as authCreators } from '~/store/ducks/auth'
import { TextInputMask } from 'react-native-masked-text'
import colors from '~/styles/colors'
import { tostFormErrors } from '~/common/helpers/formValidationMessage'
import * as yup from 'yup'
import styles from './styles'

const size = PixelRatio.getPixelSizeForLayoutSize

class SignInForm extends React.Component {
  renderPhone = (maskedPhone, setFieldValue, error) => (
    <TextInputMask
      placeholderTextColor={colors.hightLightText}
      placeholder="Telefone com DDD"
      includeRawValueInChangeText
      type="cel-phone"
      options={{
        maskType: 'BRL',
        withDDD: true,
        dddMask: '(99) '
      }}
      onChangeText={(newMaskedPhone, rawPhone) => {
        setFieldValue('maskedPhone', newMaskedPhone)
        setFieldValue('phone', rawPhone)
      }}
      value={maskedPhone ? `${maskedPhone}` : ''}
      style={[{ ...styles.textInput, marginBottom: size(10) }, error && styles.inputError]}
    />
  )

  renderPassword = (password, setFieldValue, error) => (
    <TextInput
      placeholderTextColor={colors.hightLightText}
      value={password}
      style={[{ ...styles.textInput, marginBottom: size(10) }, error && styles.inputError]}
      placeholder="Senha"
      textContentType="password"
      secureTextEntry
      underlineColorAndroid="transparent"
      onChangeText={text => setFieldValue('password', text)}
    />
  )

  render() {
    const {
      errors, handleSubmit, values, setFieldValue, isSubmitting
    } = this.props
    if (isSubmitting) tostFormErrors(errors)
    return (
      <KeyboardAvoidingView style={styles.formContainer} behavior="padding" enabled>
        <View style={{ flexDirection: 'row' }}>
          {this.renderPhone(values.maskedPhone, setFieldValue, errors.phone)}
        </View>

        <View style={{ flexDirection: 'row' }}>
          {this.renderPassword(values.password, setFieldValue, errors.password)}
        </View>

        <View style={{ flexDirection: 'row' }}>
          <RkButton
            style={styles.button}
            contentStyle={{ color: colors.hightLightText }}
            onPress={() => handleSubmit()}
          >
            ENTRAR
          </RkButton>
        </View>
      </KeyboardAvoidingView>
    )
  }
}

const signInForm = withFormik({
  mapPropsToValues: () => ({
    maskedPhone: '',
    phone: '',
    password: ''
  }),

  handleSubmit: (credentials, form) => {
    form.props.login({ ...credentials, maskedPhone: undefined })
  },
  validationSchema: yup.object().shape({
    phone: yup
      .string('Informe o telefone')
      .length(11, 'informe um telefone válido')
      .required('preencha o campo telefone'),
    password: yup
      .string('informe a senha')
      .required('informe uma senha')
      .min(6, 'A senha deve conter no mínimo 6 dígitos')
  }),
  isInitialValid: false,
  validateOnBlur: false,
  validateOnChange: false
})(SignInForm)

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    login: authCreators.asyncSignIn
  },
  dispatch
)

export default connect(
  null,
  mapDispatchToProps
)(signInForm)
