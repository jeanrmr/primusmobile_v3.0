import React, { Component } from 'react'
import {
  View, Image, ImageBackground, TouchableOpacity, Text, SafeAreaView
} from 'react-native'

import { RkButton } from 'react-native-ui-kitten'
import SignInForm from './signInForm'
import styles, { colors, sizes } from './styles'

export class LoginScreen extends Component {
  static navigationOptions = {
    header: null
  }

  render() {
    const { navigation } = this.props
    return (
      <View style={styles.parent}>
        <ImageBackground
          blurRadius={sizes.blur}
          source={require('../../../assets/login_background_cut.jpg')}
          style={styles.backgroundImage}
        >
          <View style={styles.loginContainer}>
            <SafeAreaView>
              <View style={styles.loginForm}>
                <Image
                  source={require('../../../assets/logo2.png')}
                  style={styles.logo}
                  resizeMode="contain"
                />

                <SignInForm navigation={navigation} />

                <View>
                  <TouchableOpacity
                    style={{
                      marginTop: 20,
                      alignItems: 'center'
                    }}
                  >
                    <Text>Esqueceu sua senha?</Text>
                    <Text style={{ fontWeight: 'bold' }}>Solicite uma nova</Text>
                  </TouchableOpacity>
                </View>

                <View style={{ flexDirection: 'row', marginTop: 20 }}>
                  <RkButton
                    style={{
                      flex: 0.8,
                      backgroundColor: '#4266B2',
                      borderRadius: 20
                    }}
                    contentStyle={{ color: colors.white }}
                    onPress={() => {}}
                  >
                    Entrar com Facebook
                  </RkButton>
                </View>

                <TouchableOpacity
                  style={{
                    flexDirection: 'row',
                    marginTop: 10,
                    justifyContent: 'center',
                    marginBottom: 20
                  }}
                  onPress={() => navigation.navigate('signUp')}
                >
                  <Text>Ainda não tem cadastro?</Text>
                  <Text style={{ fontWeight: 'bold' }}> Cadastrar-se!</Text>
                </TouchableOpacity>
              </View>
            </SafeAreaView>
          </View>
        </ImageBackground>
      </View>
    )
  }
}

export default LoginScreen
