import { StyleSheet } from 'react-native'
import { colors, sizes } from '~/styles'

export { colors, sizes }

export default StyleSheet.create({
  parent: {
    flex: 1,
    backgroundColor: colors.backgroundDefault
  },
  imageContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  backgroundImage: {
    width: '100%',
    height: '100%'
  },
  loginContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  loginForm: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  logo: {
    flex: 1
  },
  formContainer: {
    flexDirection: 'column',
    alignItems: 'center'
  },
  textInput: {
    backgroundColor: colors.blackTransparent,
    borderRadius: 20,
    color: colors.hightLightText,
    height: 40,
    flex: 0.8,
    textAlign: 'center'
  },
  button: {
    flex: 0.8,
    borderRadius: 20,
    backgroundColor: colors.backgroundDefault
  },
  inputError: {
    borderColor: colors.danger,
    borderWidth: 0.5
  }
})
