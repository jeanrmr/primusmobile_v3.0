import React from 'react'
import PropTypes from 'prop-types'
import {
  View, Image, PixelRatio, StyleSheet
} from 'react-native'
import { ifIphoneX } from 'react-native-iphone-x-helper'
import colors from '../../styles/colors'

const size = PixelRatio.getPixelSizeForLayoutSize

const HeaderContent = () => (
  <View style={styles.container}>
    <Image
      source={require('../../../assets/profile.jpg')}
      style={{
        width: 100,
        height: 100,
        borderRadius: 50,
        borderWidth: 4,
        borderColor: colors.hightLightText
      }}
    />
  </View>
)

const styles = StyleSheet.create({
  label: {
    color: colors.label
  },
  container: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    ...ifIphoneX({ marginTop: size(15) }, { marginTop: size(25) })
  }
})

HeaderContent.propTypes = {
  user: PropTypes.string
}

export default HeaderContent
