import React from 'react'
import PropTypes from 'prop-types'
import {
  View, Image, Text, PixelRatio, StyleSheet
} from 'react-native'
import { ifIphoneX } from 'react-native-iphone-x-helper'
import colors from '../../styles/colors'

const size = PixelRatio.getPixelSizeForLayoutSize

const HeaderContent = props => (
  <View style={styles.container}>
    <Image
      source={require('../../../assets/profile.jpg')}
      style={{
        width: 100,
        height: 100,
        borderRadius: 50,
        borderWidth: 4,
        borderColor: colors.white
      }}
    />

    <View style={{ alignContent: 'center', justifyContent: 'center' }}>
      <View style={{ flexDirection: 'row', justifyContent: 'center', marginBottom: 10 }}>
        <Text style={{ ...styles.label, fontSize: 20 }}>{props.user}</Text>
      </View>

      <View style={{ flexDirection: 'row', justifyContent: 'center', marginBottom: 10 }}>
        <Text style={{ ...styles.label, fontSize: 20 }}>{props.role}</Text>
      </View>
    </View>
  </View>
)

const styles = StyleSheet.create({
  label: {
    color: colors.label
  },
  container: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginLeft: size(10),
    marginRight: size(10),
    ...ifIphoneX({ marginTop: size(20) }, { marginTop: size(30) })
  }
})

HeaderContent.propTypes = {
  user: PropTypes.string
}

HeaderContent.defaultProps = {
  user: null
}

export default HeaderContent
