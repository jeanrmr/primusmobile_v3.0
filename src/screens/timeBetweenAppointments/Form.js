import React from 'react'
import { View } from 'react-native'
import {
  Form, Item, Input, Label
} from 'native-base'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { RkButton } from 'react-native-ui-kitten'
import { Creators as serviceCreators } from '~/store/ducks/service'
import colors from '~/styles/colors'
import { withFormik } from 'formik'
import * as yup from 'yup'
import { TextInputMask } from 'react-native-masked-text'
import styles from './styles'
import { tostFormErrors } from '~/common/helpers/formValidationMessage'
import FormItem from '~/common/components/formItem'

const EditTimeBetweenForm = (props) => {
  const {
    handleSubmit, errors, setFieldValue, values, isSubmitting
  } = props
  if (isSubmitting) tostFormErrors(errors)
  return (
    <Form>
      <FormItem name="name" errors={errors}>
        <Label style={{ color: colors.secondaryText }}>Minutos: </Label>
        <Input
          style={{ color: colors.hightLightText }}
          returnKeyType="next"
          onChangeText={text => setFieldValue('name', text)}
          value={values.name}
        />
      </FormItem>
      <View style={styles.navButtonsContainer}>
        <View style={{ alignItems: 'center', flex: 1 }}>
          <RkButton
            style={styles.formSubmitButton}
            contentStyle={{ color: colors.hightLightText }}
            onPress={() => handleSubmit()}
          >
            Salvar
          </RkButton>
        </View>
      </View>
    </Form>
  )
}

const editTimeBetweenForm = withFormik({
  mapPropsToValues: props => ({}),
  handleSubmit: (service, form) => {
    const newService = { ...service, priceFixed: undefined }
    form.props.save(newService)
  },
  validationSchema: yup.object().shape({
    name: yup.string().required('preencha o campo minutos')
  })
})(EditTimeBetweenForm)

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch)

const mapStateToProps = state => ({})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(editTimeBetweenForm)
