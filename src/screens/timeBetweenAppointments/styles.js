import { StyleSheet, PixelRatio } from 'react-native'
import colors from '~/styles/colors'
import { ifIphoneX } from 'react-native-iphone-x-helper'

const size = PixelRatio.getPixelSizeForLayoutSize

export default StyleSheet.create({
  container: {
    backgroundColor: colors.backgroundDefault
  },
  navButtonsContainer: {
    marginTop: 30,
    flexDirection: 'row',
    ...ifIphoneX({ alignItems: 'flex-end' }, { alignItems: 'center' }),
    ...ifIphoneX({ height: size(20) }, { height: size(30) }),
    marginBottom: 15
  },
  formSubmitButton: {
    backgroundColor: colors.backgroundDefault,
    borderRadius: 10,
    width: '50%',
    borderColor: colors.hightLightText
  }
})
