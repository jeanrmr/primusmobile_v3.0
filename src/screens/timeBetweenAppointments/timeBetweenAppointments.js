import React from 'react'
import { Text, View } from 'react-native'
import BackButton from '~/common/components/backButton/backButton'
import colors from '~/styles/colors'
import { Container } from 'native-base'
import styles from './styles'
import genericStyles from '~/styles'
import Card from '~/common/components/Card'
import Form from './Form'

const timeBetweenAppointments = () => (
  <Container style={{ ...styles.container, padding: '3%' }}>
    <View style={{ justifyContent: 'center', marginBottom: 10 }}>
      <Text style={{ ...genericStyles.subTitle, color: colors.defaultText, alignSelf: 'center' }}>
        Intervalo entre atendimentos
      </Text>
    </View>
    <Card>
      <Form />
    </Card>
  </Container>
)

timeBetweenAppointments.navigationOptions = {
  title: 'Configurações',
  headerLeft: <BackButton />,
  headerStyle: {
    backgroundColor: colors.highBackground
  },
  headerTitleStyle: {
    color: colors.hightLightText
  }
}

export default timeBetweenAppointments
