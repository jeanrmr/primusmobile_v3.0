import React from 'react'
import { shallow } from 'enzyme'
import { timeBetweenAppointments } from './timeBetweenAppointments'

describe('render', () => {
  const wrapper = shallow(<timeBetweenAppointments />)
  it('match snap', () => {
    expect(wrapper).toMatchSnapshot()
  })
})
