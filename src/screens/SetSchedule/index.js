import React, { Component } from 'react'
import { StyleSheet } from 'react-native'
import { Container } from 'native-base'
import Header from './Header'
import colors from '../../styles/colors'
import ScheduleTopNavigation from '../../navigation/ScheduleTopTab'

export default class SetSchedule extends Component {
  static router = ScheduleTopNavigation.router

  render() {
    const { navigation } = this.props
    return (
      <Container style={style.container}>
        <Header navigation={navigation} />
        <ScheduleTopNavigation navigation={navigation} />
      </Container>
    )
  }
}

const style = StyleSheet.create({
  container: {
    backgroundColor: colors.backgroundDefault
  }
})
