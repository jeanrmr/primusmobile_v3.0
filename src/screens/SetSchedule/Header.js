import React from 'react'
import {
  View, TouchableOpacity, Text, PixelRatio, StyleSheet
} from 'react-native'
import { Icon } from 'native-base'
import { ifIphoneX } from 'react-native-iphone-x-helper'
import colors from '../../styles/colors'

const size = PixelRatio.getPixelSizeForLayoutSize

export default (Header = (props) => {
  navigation = props.navigation

  handleBackPressed = () => {
    navigation.goBack()
  }

  return (
    <View style={styles.container}>
      <View
        style={{
          justifyContent: 'space-between',
          height: 30,
          flexDirection: 'row'
        }}
      >
        <View
          style={{
            alignSelf: 'center',
            position: 'absolute',
            flexDirection: 'row',
            width: '100%',
            alignItems: 'center',
            justifyContent: 'center',
            height: '100%'
          }}
        >
          <Text style={{ ...styles.label, fontSize: 13, fontWeight: 'bold' }}>
            AGENDAR ATENDIMENTO
          </Text>
        </View>

        <TouchableOpacity
          style={{
            alignSelf: 'center',
            position: 'absolute',
            flexDirection: 'row',
            alignItems: 'center',
            height: '100%',
            marginLeft: size(5)
          }}
          onPress={handleBackPressed}
        >
          <Icon name="arrow-round-back" style={{ ...styles.label }} />
        </TouchableOpacity>
      </View>
    </View>
  )
})

const styles = StyleSheet.create({
  label: {
    color: colors.label
  },
  container: {
    ...ifIphoneX({ paddingTop: 30 }),
    backgroundColor: colors.highBackground
  }
})
