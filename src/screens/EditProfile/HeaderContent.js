import React from 'react'
import PropTypes from 'prop-types'
import {
  Image, PixelRatio, StyleSheet, TouchableOpacity
} from 'react-native'
import { ifIphoneX } from 'react-native-iphone-x-helper'
import ImagePicker from 'react-native-image-picker'
import colors from '../../styles/colors'

const size = PixelRatio.getPixelSizeForLayoutSize

const HeaderContent = () => (
  <TouchableOpacity style={styles.container} onPress={() => handleImageClick()}>
    <Image
      source={require('../../../assets/profile.jpg')}
      style={{
        width: 100,
        height: 100,
        borderRadius: 50,
        borderWidth: 4,
        borderColor: colors.hightLightText
      }}
    />
  </TouchableOpacity>
)

handleImageClick = () => {
  ImagePicker.showImagePicker(
    {
      title: 'Selecione uma imagem'
    },
    (upload) => {}
  )
}

const styles = StyleSheet.create({
  label: {
    color: colors.label
  },
  container: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    ...ifIphoneX({ marginTop: size(15) }, { marginTop: size(25) })
  }
})

HeaderContent.propTypes = {
  user: PropTypes.string
}

export default HeaderContent
