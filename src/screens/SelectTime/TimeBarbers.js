import React, { Component } from 'react'
import {
  StyleSheet, View, Image, Text, FlatList, TouchableOpacity
} from 'react-native'
import moment from 'moment'
import { ifIphoneX } from 'react-native-iphone-x-helper'
import colors from '../../styles/colors'

export default class TimeBarbers extends Component {
  /* MOCK DATA */
  getData() {
    const data = [
      {
        photo: require('../../../assets/mock_barbers_pics/marcelo.png'),
        times: []
      },
      {
        photo: require('../../../assets/mock_barbers_pics/marcelo.png'),
        times: []
      },
      {
        photo: require('../../../assets/mock_barbers_pics/marcelo.png'),
        times: []
      },
      {
        photo: require('../../../assets/mock_barbers_pics/marcelo.png'),
        times: []
      },
      {
        photo: require('../../../assets/mock_barbers_pics/marcelo.png'),
        times: []
      }
    ]
    data.forEach((e) => {
      const m = moment()
        .millisecond(0)
        .second(0)
        .minute(0)
        .hour(8)
      for (let index = 0; index < 8; index++) {
        m.add(10, 'm')
        e.times.push(m.format('HH:mm'))
      }
    })
    return data
  }
  /* MOCK DATA */

  renderTimeItem = (time, { toggleModal }) => (
    <View style={styles.timeContainer}>
      <TouchableOpacity onPress={() => toggleModal()}>
        <Text style={styles.time}>{time}</Text>
      </TouchableOpacity>
    </View>
  )

  renderTimeLineList = timeLine => (
    <View style={styles.borderBottom}>
      <View style={styles.container}>
        <View style={styles.photoContainer}>
          <Image style={styles.photo} source={timeLine.photo} />
        </View>

        <FlatList
          data={timeLine.times}
          horizontal
          renderItem={({ item }) => this.renderTimeItem(item, this.props)}
        />
      </View>
    </View>
  )

  render() {
    return (
      <FlatList
        style={{ ...ifIphoneX({ marginBottom: 30 }, { marginBottom: 10 }) }}
        data={this.getData()}
        renderItem={({ item }) => this.renderTimeLineList(item)}
      />
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    marginTop: 10,
    marginLeft: 10,
    marginBottom: 10
  },
  photoContainer: {
    marginRight: 10
  },
  photo: {
    width: 60,
    height: 60,
    borderRadius: 30
  },
  timeContainer: {
    alignItems: 'center',
    marginRight: 10,
    flexDirection: 'row'
  },
  time: {
    borderColor: 'white',
    borderWidth: 1,
    padding: 8,
    borderRadius: 5,
    color: colors.defaultText
  },
  borderBottom: {
    borderColor: 'white',
    borderBottomWidth: 0.5
  }
})
