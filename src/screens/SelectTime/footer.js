import React from 'react'
import { View, PixelRatio, StyleSheet } from 'react-native'
import { RkButton } from 'react-native-ui-kitten'
import colors from '../../styles/colors'

const size = PixelRatio.getPixelSizeForLayoutSize

const footer = props => (
  <View style={styles.navButtonsContainer}>
    <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'space-between' }}>
      <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'space-between' }}>
        <RkButton
          style={{
            ...styles.navButton,
            marginLeft: size(2)
          }}
          contentStyle={{ color: colors.hightLightText }}
          onPress={() => {
            props.navigation.navigate('barber')
          }}
        >
          Voltar
        </RkButton>

        <RkButton
          style={{
            ...styles.navButton,
            marginRight: size(2)
          }}
          contentStyle={{ color: colors.hightLightText }}
          onPress={() => {}}
        >
          Finalizar
        </RkButton>
      </View>
    </View>
  </View>
)

const styles = StyleSheet.create({
  navButton: {
    backgroundColor: colors.backgroundDefault,
    borderRadius: 10,
    width: '40%',
    borderColor: colors.hightLightText
  },
  navButtonsContainer: {
    flexDirection: 'row',
    height: size(30),
    alignItems: 'center',
    borderTopWidth: 1,
    borderColor: colors.secondaryText,
    backgroundColor: colors.highBackground
  }
})

export default footer
