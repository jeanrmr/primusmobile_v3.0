import React, { Component } from 'react'
import { View, Text, StyleSheet } from 'react-native'
import CalendarPicker from 'react-native-calendar-picker'
import { Icon } from 'native-base'
import { RkButton } from 'react-native-ui-kitten'
import colors from '~/styles/colors'
import TimeBarbers from './TimeBarbers'
import Modal from '~/common/components/Modal'

const weekdays = ['Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom']
const months = [
  'Janeiro',
  'Fevereiro',
  'Março',
  'Abril',
  'Maio',
  'Junho',
  'Julho',
  'Agosto',
  'Setembro',
  'Outubro',
  'Novembro',
  'Dezembro'
]

export default class SelectTime extends Component {
  state = {
    isModalVisible: false
  }

  constructor(...args) {
    super(...args)
    this.toggleModal = this.toggleModal.bind(this)
  }

  toggleModal = () => this.setState(prevState => ({ isModalVisible: !prevState.isModalVisible }))

  renderModalBody = () => (
    <View style={styles.modalContainer}>
      <View style={{ flexDirection: 'column', flex: 1 }}>
        <View>
          <View style={styles.infoItem}>
            <Icon
              name="time"
              style={{ color: colors.defaultText, fontSize: 20, marginRight: 10 }}
            />
            <Text style={{ color: colors.defaultText }}> 12:00 - 13:00</Text>
          </View>

          <View style={styles.infoItem}>
            <Icon
              name="calendar"
              style={{ color: colors.defaultText, fontSize: 20, marginRight: 10 }}
            />
            <Text style={{ color: colors.defaultText }}> Segunda, 5 de março de 2019</Text>
          </View>
        </View>

        <View style={styles.profNameContainer}>
          <View style={{ flexDirection: 'row', alignContent: 'center' }}>
            <Text style={{ color: colors.defaultText, fontWeight: 'bold' }}>Marcelo Moraes</Text>
          </View>
        </View>

        <View style={styles.serviceDetailContainer}>
          <View style={styles.serviceHeader}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <View style={{ flex: 1 }}>
                <Text style={{ color: colors.defaultText }}>Serviço</Text>
              </View>

              <View style={{ flex: 1, alignItems: 'center' }}>
                <Text style={{ color: colors.defaultText }}>Tempo</Text>
              </View>

              <View style={{ flex: 1, alignItems: 'flex-end' }}>
                <Text style={{ color: colors.defaultText }}>Valor</Text>
              </View>
            </View>
          </View>

          <View style={styles.serviceContent}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <View style={{ flex: 1 }}>
                <Text style={{ color: colors.defaultText }}>Corte</Text>
              </View>

              <View style={{ flex: 1, alignItems: 'center' }}>
                <Text style={{ color: colors.defaultText }}>30 min</Text>
              </View>

              <View style={{ flex: 1, alignItems: 'flex-end' }}>
                <Text style={{ color: colors.defaultText }}>40,00</Text>
              </View>
            </View>

            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <View style={{ flex: 1 }}>
                <Text style={{ color: colors.defaultText }}>Barba</Text>
              </View>

              <View style={{ flex: 1, alignItems: 'center' }}>
                <Text style={{ color: colors.defaultText }}>20 min</Text>
              </View>

              <View style={{ flex: 1, alignItems: 'flex-end' }}>
                <Text style={{ color: colors.defaultText }}>30,00</Text>
              </View>
            </View>
          </View>

          <View style={styles.serviceTotalContainer}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <Text style={{ color: colors.defaultText }}>Total </Text>
              <Text style={{ color: colors.hightLightText, fontWeight: 'bold' }}>50 min </Text>
              <Text style={{ color: colors.hightLightText, fontWeight: 'bold' }}>70,00 </Text>
            </View>
          </View>
        </View>

        <View style={{ alignItems: 'center', marginTop: 20 }}>
          <RkButton
            style={{
              backgroundColor: colors.hightLightText,
              borderRadius: 10,
              width: '50%',
              borderWidth: 0.5,
              borderColor: colors.hightLightText
            }}
            contentStyle={{ color: colors.highBackground }}
            onPress={() => {
              this.toggleModal()
            }}
          >
            Agendar
          </RkButton>
        </View>
      </View>
    </View>
  )

  render() {
    const { isModalVisible } = this.state
    return (
      <View style={{ flex: 1 }}>
        <CalendarPicker
          textStyle={{ color: colors.defaultText }}
          weekdays={weekdays}
          months={months}
          previousTitle="Anterior"
          nextTitle="Próximo"
          selectedDayColor={colors.hightLightText}
          startFromMonday
        />
        <View style={{ borderBottomColor: 'white', borderWidth: 1 }} />

        <TimeBarbers toggleModal={this.toggleModal} />

        <Modal isVisible={isModalVisible} toggleModal={this.toggleModal}>
          {this.renderModalBody()}
        </Modal>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    flexDirection: 'row'
  },
  profNameContainer: {
    alignItems: 'center',
    padding: 10,
    borderBottomWidth: 0.5,
    borderTopWidth: 0.5,
    borderColor: 'white',
    marginTop: 10
  },
  serviceDetailContainer: {
    flex: 1
  },
  serviceTotalContainer: {
    borderBottomWidth: 0.5,
    borderTopWidth: 0.5,
    borderColor: 'white',
    height: 20
  },
  serviceHeader: {
    borderBottomWidth: 0.5,
    borderColor: 'white',
    marginTop: 5,
    marginBottom: 5
  },
  serviceContent: {
    flex: 1
  },
  infoItem: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 5
  }
})
