import { StyleSheet, PixelRatio } from 'react-native'
import colors from '~/styles/colors'
import { ifIphoneX } from 'react-native-iphone-x-helper'
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen'

const size = PixelRatio.getPixelSizeForLayoutSize

export default StyleSheet.create({
  container: {
    backgroundColor: colors.backgroundCard,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    paddingTop: '25%'
  },
  logo: {
    width: wp('55%'),
    height: wp('45%')
  },
  navButtonsContainer: {
    marginTop: hp('1%'),
    flexDirection: 'row',
    ...ifIphoneX({ alignItems: 'flex-end' }, { alignItems: 'center' }),
    ...ifIphoneX({ height: size(20) }, { height: size(30) }),
    marginBottom: 15
  },
  formContainer: {
    flex: 1,
    flexDirection: 'row'
  },
  fildsContainer: {
    width: '100%'
  },
  form: {
    paddingRight: '5%'
  },
  submitButton: {
    backgroundColor: colors.backgroundDefault,
    borderRadius: 10,
    width: '50%',
    borderColor: colors.hightLightText
  }
})
