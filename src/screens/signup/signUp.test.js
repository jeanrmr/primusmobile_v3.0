import React from '~/screens/signup/node_modules/react'
import { shallow } from '~/screens/signup/node_modules/enzyme'
import SignUp from './SignUp'

describe('render', () => {
  const wrapper = shallow(<SignUp />)
  it('match snap', () => {
    expect(wrapper).toMatchSnapshot()
  })
})
