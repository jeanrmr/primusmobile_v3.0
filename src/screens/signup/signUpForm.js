import { withFormik } from 'formik'
import React, { Component } from 'react'
import { View } from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { TextInputMask } from 'react-native-masked-text'
import colors from '~/styles/colors'
import {
  Form, Item, Label, Input
} from 'native-base'
import FormItem from '~/common/components/formItem'
import { RkButton } from 'react-native-ui-kitten'
import * as yup from 'yup'
import reactotron from 'reactotron-react-native'
import styles from './styles'
import { tostFormErrors } from '~/common/helpers/formValidationMessage'

export class SignUpForm extends Component {
  phoneField

  renderPhone = (maskedPhone, setFieldValue) => (
    <TextInputMask
      placeholderTextColor={colors.hightLightText}
      includeRawValueInChangeText
      customTextInput={Input}
      type="cel-phone"
      options={{
        maskType: 'BRL',
        withDDD: true,
        dddMask: '(99) '
      }}
      onChangeText={(newMaskedPhone, rawPhone) => {
        setFieldValue('maskedPhone', newMaskedPhone)
        setFieldValue('phone', rawPhone)
      }}
      value={maskedPhone ? `${maskedPhone}` : ''}
      style={{ color: colors.hightLightText }}
      ref={(ref) => {
        this.phoneField = ref
      }}
    />
  )

  render() {
    const {
      handleSubmit, values, setFieldValue, errors, isSubmitting
    } = this.props
    if (isSubmitting) tostFormErrors(errors)
    return (
      <View>
        <View style={{ flexDirection: 'column' }}>
          <Form style={styles.form}>
            <FormItem errors={errors} fieldName="firstName">
              <Label style={{ color: colors.secondaryText }}>Nome</Label>

              <Input
                onChangeText={(firstName) => {
                  setFieldValue('firstName', firstName)
                }}
                value={values.firstName}
                style={{ color: colors.hightLightText }}
                returnKeyType="next"
              />
            </FormItem>

            <FormItem errors={errors} fieldName="lastName">
              <Label style={{ color: colors.secondaryText }}>Sobrenome</Label>
              <Input
                onChangeText={(lastName) => {
                  setFieldValue('lastName', lastName)
                }}
                value={values.lastName}
                style={{ color: colors.hightLightText }}
                returnKeyType="next"
              />
            </FormItem>

            <FormItem errors={errors} fieldName="email">
              <Label style={{ color: colors.secondaryText }}>email</Label>
              <Input
                onChangeText={(email) => {
                  setFieldValue('email', email)
                }}
                style={{ color: colors.hightLightText }}
                returnKeyType="next"
                value={values.email}
              />
            </FormItem>

            <FormItem errors={errors} fieldName="phone">
              <Label style={{ color: colors.secondaryText }}>Telefone</Label>
              {this.renderPhone(values.maskedPhone, setFieldValue)}
            </FormItem>

            <FormItem errors={errors} fieldName="password">
              <Label style={{ color: colors.secondaryText }}>Senha</Label>
              <Input
                textContentType="password"
                secureTextEntry
                style={{ color: colors.hightLightText }}
                returnKeyType="next"
                onChangeText={(password) => {
                  setFieldValue('password', password)
                }}
                value={values.password}
              />
            </FormItem>

            <Item
              style={{
                borderBottomColor: colors.hightLightText,
                ...(errors.passwordConfirmation && { borderBottomColor: 'red' })
              }}
            >
              <Label style={{ color: colors.secondaryText }}>Repetir senha</Label>
              <Input
                textContentType="password"
                secureTextEntry
                style={{ color: colors.hightLightText }}
                returnKeyType="next"
                onChangeText={(passwordConfirmation) => {
                  setFieldValue('passwordConfirmation', passwordConfirmation)
                }}
                value={values.passwordConfirmation}
              />
            </Item>
          </Form>
        </View>
        <View style={styles.navButtonsContainer}>
          <View style={{ alignItems: 'center', flex: 1 }}>
            <RkButton
              style={styles.submitButton}
              contentStyle={{ color: colors.hightLightText }}
              onPress={() => handleSubmit()}
            >
              Cadastrar!
            </RkButton>
          </View>
        </View>
      </View>
    )
  }
}
const signUpForm = withFormik({
  mapPropsToValues: () => ({}),

  handleSubmit: (formValues, form) => {
    const finalUser = formValues
    delete finalUser.maskedPhone
    delete finalUser.passwordConfirmation
    reactotron.log(finalUser)
  },
  validationSchema: yup.object().shape({
    firstName: yup.string().required('preencha o campo nome'),
    lastName: yup.string().required('preencha o campo sobrenome'),
    email: yup
      .string()
      .email('email invalido')
      .required('preencha o campo email'),
    phone: yup
      .string()
      .length(11, 'informe um telefone válido')
      .required('preencha o campo telefone'),
    password: yup
      .string()
      .required('informe uma senha')
      .min(6, 'A senha deve conter no mínimo 6 dígitos'),
    passwordConfirmation: yup
      .string()
      .required('Confirme a senha')
      .oneOf([yup.ref('password'), null], 'As senhas não conferem')
  }),
  isInitialValid: false,
  validateOnBlur: false,
  validateOnChange: false
})(SignUpForm)

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    // showMessage: Message.showMessage
  },
  dispatch
)

export default connect(
  null,
  mapDispatchToProps
)(signUpForm)
