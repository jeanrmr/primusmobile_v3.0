import { View, KeyboardAvoidingView, Image } from 'react-native'
import { Container } from 'native-base'
import colors from '~/styles/colors'
import React, { Component } from 'react'
import styles from './styles'
import BackButton from '~/common/components/backButton'
import SignUpForm from './signUpForm'

export default class SignUp extends Component {
  static navigationOptions = () => ({
    header: null,
    headerStyle: {
      backgroundColor: colors.highBackground
    }
  })

  render() {
    return (
      <Container style={styles.container}>
        <KeyboardAvoidingView behavior="position" enabled>
          <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
            <Image
              source={require('../../../assets/logo2.png')}
              resizeMode="cover"
              style={styles.logo}
            />
          </View>
          <SignUpForm />
        </KeyboardAvoidingView>
        <BackButton />
      </Container>
    )
  }
}
