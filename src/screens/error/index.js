import React from 'react'
import { View, Text } from 'react-native'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Creators as errorAction } from '~/store/ducks/error'
import styles from './style'

export class Error extends React.Component {
  componentDidCatch(error, info) {
    const { showError } = this.props
    showError(error, info)
  }

  render() {
    const { error, children } = this.props
    if (error.error) {
      return (
        <View style={styles.container}>
          <Text>error!</Text>
        </View>
      )
    }
    return children
  }
}

const mapStateToProps = state => ({
  error: state.error
})

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    showError: errorAction.showError
  },
  dispatch
)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Error)
