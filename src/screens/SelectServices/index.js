import React, { Component } from 'react'
import {
  View, PixelRatio, StyleSheet, SafeAreaView
} from 'react-native'
import { RkButton } from 'react-native-ui-kitten'
import { ifIphoneX } from 'react-native-iphone-x-helper'
import ServiceList from './ServiceList'
import colors from '../../styles/colors'

const size = PixelRatio.getPixelSizeForLayoutSize

export default class SelectService extends Component {
  serviceList = [
    {
      name: 'Corte',
      price: 30.0,
      time: 30,
      checked: false
    },
    {
      name: 'Barba',
      price: 30.0,
      time: 30,
      checked: false
    },
    {
      name: 'Corte/Barba',
      price: 50.0,
      time: 60,
      checked: false
    },
    {
      name: 'Selagem',
      price: 60.0,
      time: 50,
      checked: false
    },
    {
      name: 'Corte/Selagem',
      price: 80.0,
      time: 50,
      checked: false
    },
    {
      name: 'Corte/Selagem/Barba',
      price: 100.0,
      time: 90,
      checked: false
    }
  ]

  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={{ flex: 1 }}>
          <ServiceList serviceList={this.serviceList} />
        </View>

        <SafeAreaView style={{ backgroundColor: colors.highBackground }}>
          <View style={styles.navButtonsContainer}>
            <View style={{ alignItems: 'center', flex: 1 }}>
              <RkButton
                style={{
                  backgroundColor: colors.backgroundDefault,
                  borderRadius: 10,
                  width: '50%',
                  borderColor: colors.hightLightText
                }}
                contentStyle={{ color: colors.hightLightText }}
                onPress={() => {
                  this.props.navigation.navigate('barber')
                }}
              >
                Continuar
              </RkButton>
            </View>
          </View>
        </SafeAreaView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  navButtonsContainer: {
    flexDirection: 'row',
    ...ifIphoneX({ alignItems: 'flex-end' }, { alignItems: 'center' }),
    ...ifIphoneX({ height: size(20) }, { height: size(30) }),
    borderTopWidth: 1,
    borderColor: colors.secondaryText,
    backgroundColor: colors.highBackground
  }
})
