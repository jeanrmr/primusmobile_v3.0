import React from 'react'
import { TouchableOpacity } from 'react-native'
import { Icon } from 'native-base'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import colors from '~/styles/colors'
import NavigationService from '~/navigation/NavigationService'
import { Creators as serviceCreators } from '~/store/ducks/service'

const buttonNewService = (props) => {
  const { cleanEditService } = props
  return (
    <TouchableOpacity
      transparent
      onPress={() => {
        cleanEditService()
        NavigationService.navigate('editService')
      }}
      style={{ marginRight: 10 }}
    >
      <Icon name="add" style={{ color: colors.hightLightText }} />
    </TouchableOpacity>
  )
}

export default connect(
  null,
  dispatch => bindActionCreators(
    {
      cleanEditService: serviceCreators.cleanEditService
    },
    dispatch
  )
)(buttonNewService)
