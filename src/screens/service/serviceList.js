import React, { Component } from 'react'
import {
  Text, TouchableOpacity, View, FlatList
} from 'react-native'
import { Icon, Container } from 'native-base'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import colors from '~/styles/colors'
import NavigationService from '~/navigation/NavigationService'
import { Creators as serviceCreators } from '~/store/ducks/service'
import ButtonNewService from './buttonNewService'
import styles from './styles'

export class Service extends Component {
  static navigationOptions = () => ({
    title: 'Serviços',
    headerLeft: (
      <TouchableOpacity
        transparent
        onPress={() => NavigationService.goBack()}
        style={{ marginLeft: 10 }}
      >
        <Icon name="arrow-round-back" style={{ color: colors.hightLightText }} />
      </TouchableOpacity>
    ),
    headerRight: <ButtonNewService />,
    headerStyle: {
      backgroundColor: colors.highBackground
    },
    headerTitleStyle: {
      color: colors.hightLightText
    }
  })

  componentWillMount() {
    const { navigation, findAll } = this.props
    navigation.addListener('willFocus', () => {
      NavigationService.hideBottomBar('profile')
    })
    findAll()
  }

  renderOption = (item) => {
    const { editService } = this.props
    return (
      <TouchableOpacity
        style={styles.itemContainer}
        onPress={() => {
          editService(item)
          NavigationService.navigate('editService')
        }}
      >
        <View style={{ flexDirection: 'row', flex: 1, padding: 10 }}>
          <View style={styles.itemTextContainer}>
            <Text style={styles.itemLabel}>{item.name}</Text>
          </View>

          <View style={{ flex: 1, alignItems: 'flex-end' }}>
            <Icon name="arrow-forward" style={styles.iconItem} />
          </View>
        </View>
      </TouchableOpacity>
    )
  }

  render() {
    const { services } = this.props
    return (
      <Container style={styles.container}>
        <FlatList
          data={services}
          renderItem={({ item, index }) => this.renderOption(item, index)}
        />
      </Container>
    )
  }
}

const mapStateToProps = state => ({
  services: state.service.services
})

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    findAll: serviceCreators.asyncFindAllServices,
    editService: serviceCreators.editService,
    cleanEditService: serviceCreators.cleanEditService
  },
  dispatch
)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Service)
