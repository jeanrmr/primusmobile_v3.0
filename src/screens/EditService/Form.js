import React from 'react'
import { View } from 'react-native'
import {
  Form, Item, Input, Label
} from 'native-base'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { RkButton } from 'react-native-ui-kitten'
import { Creators as serviceCreators } from '~/store/ducks/service'
import colors from '~/styles/colors'
import { withFormik } from 'formik'
import * as yup from 'yup'
import { TextInputMask } from 'react-native-masked-text'
import styles from './styles'
import { tostFormErrors } from '~/common/helpers/formValidationMessage'
import FormItem from '~/common/components/formItem'

const EditServiceForm = (props) => {
  const {
    handleSubmit, errors, setFieldValue, values, isSubmitting
  } = props
  if (isSubmitting) tostFormErrors(errors)
  return (
    <Form>
      <FormItem name="name" errors={errors}>
        <Label style={{ color: colors.secondaryText }}>Serviço: </Label>
        <Input
          style={{ color: colors.hightLightText }}
          returnKeyType="next"
          onChangeText={text => setFieldValue('name', text)}
          value={values.name}
        />
      </FormItem>
      <Item style={{ borderBottomColor: colors.hightLightText }}>
        <Label style={{ color: colors.secondaryText }}>Preço: </Label>
        <TextInputMask
          style={{ color: colors.hightLightText }}
          type="money"
          customTextInput={Input}
          includeRawValueInChangeText
          onChangeText={(maskedText, rawText) => {
            setFieldValue('price', rawText)
            setFieldValue('priceFixed', maskedText)
          }}
          value={values.priceFixed ? `${values.priceFixed}` : ''}
        />
      </Item>
      <View style={styles.navButtonsContainer}>
        <View style={{ alignItems: 'center', flex: 1 }}>
          <RkButton
            style={styles.formSubmitButton}
            contentStyle={{ color: colors.hightLightText }}
            onPress={() => handleSubmit()}
          >
            Salvar
          </RkButton>
        </View>
      </View>
    </Form>
  )
}

const editServiceForm = withFormik({
  mapPropsToValues: props => ({
    id: props.service.id,
    name: props.service.name,
    price: props.service.price,
    priceFixed: props.service.price
  }),
  handleSubmit: (service, form) => {
    const newService = { ...service, priceFixed: undefined }
    form.props.save(newService)
  },
  validationSchema: yup.object().shape({
    name: yup.string().required('preencha o campo nome do serviço')
  })
})(EditServiceForm)

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    save: serviceCreators.asyncSaveService
  },
  dispatch
)

const mapStateToProps = state => ({
  service: state.service.editService
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(editServiceForm)
