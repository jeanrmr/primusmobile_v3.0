import React, { Component } from 'react'
import { KeyboardAvoidingView, ScrollView, TouchableOpacity } from 'react-native'
import { Container, Icon } from 'native-base'
import colors from '~/styles/colors'
import NavigationService from '~/navigation/NavigationService'
import { connect } from 'react-redux'
import Form from './Form'
import ConnectedButtonDeleteService from './ButtonDeleteService'
import styles from './styles'

export class EditService extends Component {
  static navigationOptions = () => ({
    title: 'Serviços',
    headerLeft: (
      <TouchableOpacity
        transparent
        onPress={() => NavigationService.goBack()}
        style={{ marginLeft: 10 }}
      >
        <Icon name="arrow-round-back" style={{ color: colors.hightLightText }} />
      </TouchableOpacity>
    ),
    headerRight: <ConnectedButtonDeleteService />,
    headerStyle: {
      backgroundColor: colors.highBackground
    },
    headerTitleStyle: {
      color: colors.hightLightText
    }
  })

  render() {
    return (
      <Container style={styles.container}>
        <ScrollView>
          <KeyboardAvoidingView style={styles.formContainer} behavior="padding" enabled>
            <Form />
          </KeyboardAvoidingView>
        </ScrollView>
      </Container>
    )
  }
}

const mapStateToProps = state => ({
  service: state.service.editService
})

export default connect(mapStateToProps)(EditService)
