import { StyleSheet, PixelRatio } from 'react-native'
import { colors } from '~/styles'
import { ifIphoneX } from 'react-native-iphone-x-helper'

const size = PixelRatio.getPixelSizeForLayoutSize

export default StyleSheet.create({
  container: {
    backgroundColor: colors.backgroundDefault
  },
  formContainer: {
    flex: 1,
    marginLeft: '5%',
    marginRight: '5%',
    marginBottom: '5%',
    marginTop: 10,
    paddingRight: '5%',
    paddingBottom: '5%',
    backgroundColor: 'rgba(52, 65, 77, 1)',
    borderRadius: 5,
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 3,
    elevation: 2
  },
  navButtonsContainer: {
    marginTop: 30,
    flexDirection: 'row',
    ...ifIphoneX({ alignItems: 'flex-end' }, { alignItems: 'center' }),
    ...ifIphoneX({ height: size(20) }, { height: size(30) }),
    marginBottom: 15
  },
  formSubmitButton: {
    backgroundColor: colors.backgroundDefault,
    borderRadius: 10,
    width: '50%',
    borderColor: colors.hightLightText
  }
})
