import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { Creators as serviceCreators } from '~/store/ducks/service'
import { TouchableOpacity, Alert } from 'react-native'
import { connect } from 'react-redux'
import { Icon } from 'native-base'
import colors from '~/styles/colors'

class ButtonDeleteService extends Component {
  showConfirmDialog = () => {
    const { deleteService, id } = this.props
    Alert.alert(
      '',
      'Deseja realmente excluir o serviço?',
      [
        {
          text: 'Sim',
          onPress: () => {
            deleteService(id)
          }
        },
        { text: 'cancelar', style: 'cancel' }
      ],
      { cancelable: true }
    )
  }

  render() {
    const { id } = this.props
    return id ? (
      <TouchableOpacity
        transparent
        onPress={() => this.showConfirmDialog()}
        style={{ marginRight: 10 }}
      >
        <Icon name="trash" style={{ color: colors.hightLightText }} />
      </TouchableOpacity>
    ) : null
  }
}

const ConnectedButtonDeleteService = connect(
  state => ({
    id: state.service.editService.id
  }),
  dispatch => bindActionCreators(
    {
      deleteService: serviceCreators.asyncDeleteService
    },
    dispatch
  )
)(ButtonDeleteService)

export default ConnectedButtonDeleteService
