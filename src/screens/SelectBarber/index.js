import React, { Component } from 'react'
import {
  View, PixelRatio, StyleSheet, SafeAreaView
} from 'react-native'
import { RkButton } from 'react-native-ui-kitten'
import Carousel from 'react-native-snap-carousel'
import { ifIphoneX } from 'react-native-iphone-x-helper'
import colors from '../../styles/colors'
import { sliderWidth, itemWidth } from '../../common/components/SliderEntry/style'
import SliderEntry from '../../common/components/SliderEntry'

const size = PixelRatio.getPixelSizeForLayoutSize

export default class SelectBarber extends Component {
  data = [
    {
      title: 'Todos os profissionais',
      illustration: require('../../../assets/mock_barbers_pics/todos.png')
    },
    {
      title: 'Marcelo Moraes',
      illustration: require('../../../assets/mock_barbers_pics/marcelo.png')
    },
    {
      title: 'Marcelo Moraes',
      illustration: require('../../../assets/mock_barbers_pics/marcelo.png')
    }
  ]

  _renderItemWithParallax({ item, index }, parallaxProps) {
    return (
      <View style={{ alignItems: 'center' }}>
        <SliderEntry data={item} even={false} parallax parallaxProps={parallaxProps} />
      </View>
    )
  }

  render() {
    return (
      <View style={{ flex: 1, alignContent: 'center' }}>
        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
          <Carousel
            ref={(c) => {
              this._carousel = c
            }}
            data={this.data}
            renderItem={this._renderItemWithParallax}
            sliderWidth={sliderWidth}
            itemWidth={itemWidth}
            hasParallaxImages
            inactiveSlideScale={0.94}
            inactiveSlideOpacity={0.7}
          />
        </View>

        <SafeAreaView style={{ backgroundColor: colors.highBackground }}>
          <View style={styles.navButtonsContainer}>
            <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'space-around' }}>
              <RkButton
                style={{
                  ...styles.navButton,
                  marginLeft: size(2)
                }}
                contentStyle={{ color: colors.hightLightText }}
                onPress={() => {
                  this.props.navigation.navigate('service')
                }}
              >
                Voltar
              </RkButton>
              <RkButton
                style={{
                  ...styles.navButton,
                  marginRight: size(2)
                }}
                contentStyle={{ color: colors.hightLightText }}
                onPress={() => {
                  this.props.navigation.navigate('time')
                }}
              >
                Continuar
              </RkButton>
            </View>
          </View>
        </SafeAreaView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  navButton: {
    backgroundColor: colors.backgroundDefault,
    borderRadius: 10,
    width: '40%',
    borderColor: colors.hightLightText
  },
  navButtonsContainer: {
    flexDirection: 'row',
    ...ifIphoneX({ alignItems: 'flex-end' }, { alignItems: 'center' }),
    ...ifIphoneX({ height: size(20) }, { height: size(30) }),
    borderTopWidth: 1,
    borderColor: colors.secondaryText,
    backgroundColor: colors.highBackground
  }
})
