import React from 'react'
import { Text, View, TouchableOpacity } from 'react-native'
import BackButton from '~/common/components/backButton/backButton'
import colors from '~/styles/colors'
import { Icon, Container } from 'native-base'
import styles from './styles'
import NavigationService from '~/navigation/NavigationService'

export default class settings extends React.Component {
  static navigationOptions = {
    title: 'Configurações',
    headerLeft: <BackButton />,
    headerStyle: {
      backgroundColor: colors.highBackground
    },
    headerTitleStyle: {
      color: colors.hightLightText
    }
  }

  componentWillMount = () => {
    const { navigation } = this.props
    navigation.addListener('willFocus', () => {
      NavigationService.hideBottomBar('profile')
    })
  }

  renderOption = (title, onPress) => (
    <TouchableOpacity style={styles.itemContainer} onPress={() => onPress()}>
      <View style={{ flexDirection: 'row', flex: 1, padding: 10 }}>
        <View style={styles.itemTextContainer}>
          <Text style={styles.itemLabel}>{title}</Text>
        </View>

        <View style={{ flex: 1, alignItems: 'flex-end' }}>
          <Icon name="arrow-forward" style={styles.iconItem} />
        </View>
      </View>
    </TouchableOpacity>
  )

  render() {
    return (
      <Container style={styles.container}>
        {this.renderOption('Tempo entre atendimentos', () => NavigationService.navigate('timeBetweenAppoitments'))}
        {this.renderOption('Horário de funcionamento', () => NavigationService.navigate('a'))}
        {this.renderOption('Feriado/Folga', () => NavigationService.navigate('a'))}
        {this.renderOption('Dias de funcionamento', () => NavigationService.navigate('a'))}
      </Container>
    )
  }
}
