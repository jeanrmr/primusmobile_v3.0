import { StyleSheet } from 'react-native'
import colors from '~/styles/colors'

export default StyleSheet.create({
  socialIcons: {
    color: colors.hightLightText,
    marginLeft: 10,
    fontSize: 22
  },
  iconItem: {
    color: '#ddd',
    alignItems: 'center'
  },
  container: {
    backgroundColor: colors.backgroundDefault
  },
  itemContainer: {
    borderBottomWidth: 0.5,
    flexDirection: 'row',
    height: 50,
    borderColor: colors.hightLightText
  },
  optionsContainer: {
    flex: 1
  },
  itemLabel: {
    color: '#ddd'
  },
  itemTextContainer: {
    justifyContent: 'center',
    marginLeft: 10
  }
})
