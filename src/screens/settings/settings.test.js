import React from 'react'
import { shallow } from 'enzyme'
import { settings } from './settings'

describe('render', () => {
  const wrapper = shallow(<settings />)
  it('match snap', () => {
    expect(wrapper).toMatchSnapshot()
  })
})
