import React, { Component } from 'react'
import {
  StyleSheet, ScrollView, Text, View, FlatList, Image
} from 'react-native'
import { Container, Icon } from 'native-base'
import Header from '../../common/components/Header'
import colors from '../../styles/colors'
import Card from '~/common/components/Card'
import Separator from '~/common/components/Separator'

export default class Calls extends Component {
  calls = [
    {
      date: 'Segunda, 5 de março de 2019',
      time: '10:30 - 11:00',
      profissional: 'Marcelo Morais',
      status: 'realizado',
      photo: require('../../../assets/mock_barbers_pics/marcelo.png')
    },
    {
      date: 'Segunda, 5 de março de 2019',
      time: '10:30 - 11:00',
      profissional: 'Marcelo Morais',
      status: 'realizado',
      photo: require('../../../assets/mock_barbers_pics/marcelo.png')
    },
    {
      date: 'Segunda, 5 de março de 2019',
      time: '10:30 - 11:00',
      profissional: 'Marcelo Morais',
      status: 'agendado',
      photo: require('../../../assets/mock_barbers_pics/marcelo.png')
    },
    {
      date: 'Segunda, 5 de março de 2019',
      time: '10:30 - 11:00',
      profissional: 'Marcelo Morais',
      status: 'cancelado',
      photo: require('../../../assets/mock_barbers_pics/marcelo.png')
    }
  ]

  render() {
    return (
      <Container style={style.container}>
        <ScrollView>
          <Header marginBottom={200} navigation={this.props.navigation} title="Atendimentos" />
          <View style={{ padding: 10 }}>
            <FlatList
              data={this.calls}
              renderItem={({ item, index }) => this.renderCalls(item, index)}
            />
          </View>
        </ScrollView>
      </Container>
    )
  }

  renderCalls = (item, index) => (
    <Card>
      <View style={{ flexDirection: 'row' }}>
        <View style={style.photoContainer}>
          <Image style={style.photo} source={item.photo} />
          <Text style={{ color: colors.hightLightText, fontSize: 20, fontWeight: 'bold' }}>
            {item.profissional}
          </Text>
        </View>
      </View>

      <Separator style={{ marginBottom: 10, marginTop: 10 }} />

      <View style={{ flexDirection: 'row' }}>
        <Icon
          name="calendar"
          style={{ color: colors.defaultText, fontSize: 20, marginRight: 10 }}
        />
        <Text style={{ color: colors.secondaryText }}>{item.date}</Text>
      </View>

      <View style={{ flexDirection: 'row' }}>
        <Icon name="time" style={{ color: colors.defaultText, fontSize: 20, marginRight: 10 }} />
        <Text style={{ color: colors.secondaryText }}>{item.time}</Text>
      </View>
    </Card>
  )
}

const style = StyleSheet.create({
  container: {
    backgroundColor: colors.backgroundDefault
  },
  photoContainer: {
    marginRight: 10,
    flexDirection: 'row',
    alignItems: 'center'
  },
  photo: {
    width: 60,
    height: 60,
    borderRadius: 30,
    marginRight: 20
  }
})
