import OneSignal from 'react-native-onesignal'
import AsyncStorage from '@react-native-community/async-storage'
import reactotron from 'reactotron-react-native'

const ONE_SIGNAL_APP_ID = ''

function receivedPush(push) {
  reactotron.log('received', push)
}

function openedPush(push) {
  reactotron.log('opened', push)
}

async function idsPush(push) {
  AsyncStorage.setItem('oneSignalId', push.userId)
}

async function init() {
  OneSignal.init(ONE_SIGNAL_APP_ID, { kOSSettingsKeyAutoPrompt: true })
  OneSignal.addEventListener('received', receivedPush)
  OneSignal.addEventListener('opened', openedPush)
  OneSignal.addEventListener('ids', idsPush.bind(this))
  OneSignal.configure()
}

function removeListeners() {
  OneSignal.removeEventListener('received')
  OneSignal.removeEventListener('opened')
  OneSignal.removeEventListener('ids')
}

export default {
  removeListeners,
  init
}
