/* eslint-disable no-unused-vars */
import Axios from 'axios'
import NavigationService from '~/navigation/NavigationService'
import reactotron from 'reactotron-react-native'

const SERVER = ''
const PREFIX = ''
const ENV = 'development'

export default () => {
  const url = `${SERVER}${PREFIX}`
  Axios.defaults.baseURL = url
  Axios.defaults.timeout = 5000

  Axios.defaults.validateStatus = status => status === 200

  function handleHttpErrorOnDev(error) {
    reactotron.log(error)
    if (error.response === undefined) {
      throw new Error('Não foi possível se conectar com o servidor')
    }
    if (error.response.status === 401) {
      throw new Error(error.response.data)
    }
    if (error.response.status >= 500) {
      throw new Error(error.response.data)
    }
  }

  function handleHttpErrorOnProd(error) {
    if (error.response === undefined) {
      throw new Error('Não foi possível se conectar com o servidor')
    }
    if (error.response.status === 401) {
      NavigationService.navigate('auth')
      throw new Error(error.response.data)
    }
    if (error.response.status >= 500) {
      throw new Error('Erro inesperado no servidor!')
    }
  }

  // Add a response interceptor
  Axios.interceptors.response.use(
    response => response,
    (error) => {
      if (ENV !== 'development') {
        handleHttpErrorOnProd(error)
      } else {
        handleHttpErrorOnDev(error)
      }
    }
  )
}
