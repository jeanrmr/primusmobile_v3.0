import { showMessage } from 'react-native-flash-message'

export const tostFormErrors = (errors) => {
  const errorsLenght = Object.values(errors).length
  if (errors && errorsLenght > 0) {
    let finalMessage = ''
    Object.values(errors).forEach((value) => {
      finalMessage += `\n ${value}`
    })
    const duration = errorsLenght === 1 ? 1000 : errorsLenght * 1300
    showMessage({ type: 'warning', message: finalMessage, duration })
  }
}
