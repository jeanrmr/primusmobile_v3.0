import React from 'react'
import { Item } from 'native-base'
import PropTypes from 'prop-types'
import styles from './styles'

const formItem = (props) => {
  const { errors = {}, fieldName = '', children } = props
  return <Item style={[styles.standart, errors[fieldName] && styles.error]}>{children}</Item>
}
formItem.propTypes = {
  errors: PropTypes.isRequired,
  fieldName: PropTypes.string.isRequired
}
export default formItem
