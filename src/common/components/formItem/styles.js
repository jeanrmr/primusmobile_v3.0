import { StyleSheet } from 'react-native'
import colors from '~/styles/colors'

export default StyleSheet.create({
  standart: {
    borderBottomColor: colors.hightLightText
  },
  error: {
    borderBottomColor: colors.danger
  }
})
