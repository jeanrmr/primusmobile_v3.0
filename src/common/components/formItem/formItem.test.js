import React from 'react'
import { shallow } from 'enzyme'
import { formItem } from './formItem'

describe('render', () => {
  const wrapper = shallow(<formItem />)
  it('match snap', () => {
    expect(wrapper).toMatchSnapshot()
  })
})
