import { toggle, disable, enable } from './loaderService'
import Loader from './loader'

export default Loader
export { toggle, enable, disable }
