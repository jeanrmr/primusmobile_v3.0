import { colors } from '~/styles'
import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: colors.whiteTransparent
  },
  activityIndicatorWrapper: {
    backgroundColor: colors.backgroundDefault,
    height: 100,
    width: 100,
    borderRadius: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around'
  }
})

export { colors }
