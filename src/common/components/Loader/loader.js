import React, { Component } from 'react'
import { View, Modal, ActivityIndicator } from 'react-native'
import styles, { colors } from './styles'

export default class Loader extends Component {
  constructor(props) {
    super(props)
    this.state = { loading: false }
  }

  toggle() {
    this.setState(state => ({ loading: !state.loading }))
  }

  enable() {
    this.setState(() => ({ loading: true }))
  }

  disable() {
    this.setState(() => ({ loading: false }))
  }

  render() {
    const { loading } = this.state
    return (
      <Modal visible={loading} animationType="none" transparent onRequestClose={() => {}}>
        <View style={styles.modalBackground}>
          <View style={styles.activityIndicatorWrapper}>
            <ActivityIndicator animating={loading} size="large" color={colors.hightLightText} />
          </View>
        </View>
      </Modal>
    )
  }
}
