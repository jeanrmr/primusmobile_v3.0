/* eslint-disable no-underscore-dangle */
/**
 * Non-public global class to handle the "default" FlashMessage instance to global use
 */
class LoaderMenager {
  defaultLoader = null

  register(ref) {
    this.defaultLoader = ref
  }

  unregister() {
    this.defaultLoader = null
  }

  getDefault() {
    return this.defaultLoader
  }
}

export default new LoaderMenager()
