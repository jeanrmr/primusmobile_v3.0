let loader

function setRef(ref) {
  loader = ref
}

function toggle() {
  loader.toggle()
}

function enable() {
  loader.enable()
}

function disable() {
  loader.disable()
}

export {
  setRef, toggle, enable, disable
}
