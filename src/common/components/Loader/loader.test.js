import React from 'react'
import { shallow } from 'enzyme'
import Loader from '~/common/components/loader'

describe('render tests', () => {
  const wrapper = shallow(<Loader loading />)
  it('match snap', () => {
    expect(wrapper).toMatchSnapshot()
  })

  it('visible false', () => {
    expect(wrapper.props().visible).toBe(true)
    wrapper.setProps({ loading: false })
    expect(wrapper.props().visible).toBe(false)
  })
})
