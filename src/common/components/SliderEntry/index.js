import React, { Component } from 'react'
import { View, Text, Image } from 'react-native'
import PropTypes from 'prop-types'
import { ParallaxImage } from 'react-native-snap-carousel'
import styles from './style'

export default class SliderEntry extends Component {
  static propTypes = {
    data: PropTypes.object.isRequired,
    even: PropTypes.bool,
    parallax: PropTypes.bool,
    parallaxProps: PropTypes.object
  }

  get image() {
    const {
      data: { illustration },
      parallax,
      parallaxProps,
      even
    } = this.props

    return parallax ? (
      <ParallaxImage
        source={illustration}
        containerStyle={[styles.imageContainer, even ? styles.imageContainerEven : {}]}
        style={styles.image}
        parallaxFactor={0.35}
        showSpinner
        {...parallaxProps}
      />
    ) : (
      <Image source={require('../../../../assets/logo2.png')} style={styles.image} />
    )
  }

  subtitle() {
    const {
      data: { subtitle },
      even
    } = this.props
    return subtitle ? (
      <Text style={[styles.subtitle, even ? styles.subtitleEven : {}]} numberOfLines={2}>
        {subtitle}
      </Text>
    ) : null
  }

  title() {
    const {
      data: { title },
      even
    } = this.props
    return title ? (
      <Text style={[styles.title, even ? styles.titleEven : {}]} numberOfLines={2}>
        {title.toUpperCase()}
      </Text>
    ) : null
  }

  textContainer() {
    const { even } = this.props
    title = this.title()
    subtitle = this.subtitle()
    if (title || subtitle) {
      return (
        <View>
          <View style={[styles.textContainer, even ? styles.textContainerEven : {}]}>
            {title}
            {subtitle}
          </View>
        </View>
      )
    }
  }

  render() {
    const { even } = this.props
    return (
      <View style={{ ...styles.slideInnerContainer }}>
        <View style={styles.shadow} />
        <View style={[styles.imageContainer, even ? styles.imageContainerEven : {}]}>
          {this.image}
        </View>
        {this.textContainer()}
      </View>
    )
  }
}
