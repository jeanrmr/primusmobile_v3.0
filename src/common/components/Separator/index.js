import React from 'react'
import { View } from 'react-native'
import colors from '~/styles/colors'

const Separator = (props) => {
  const { style } = props
  return (
    <View
      style={{
        flexDirection: 'row'
      }}
    >
      <View
        style={{
          borderBottomColor: '#555',
          borderBottomWidth: 1,
          shadowColor: colors.black,
          shadowOffset: { width: 0, height: 1 },
          shadowOpacity: 0.8,
          shadowRadius: 1,
          elevation: 1,
          flex: 2,
          ...style
        }}
      />
    </View>
  )
}

export default Separator
