/* eslint-disable react/forbid-prop-types */
import React from 'react'
import { View, TouchableOpacity, StyleSheet } from 'react-native'
import colors from '~/styles/colors'
import PropTypes from 'prop-types'

const Card = ({ onPress, style, children }) => {
  if (onPress) {
    return (
      <TouchableOpacity
        onPress={() => {
          onPress()
        }}
        style={{ ...styles.card, ...style }}
      >
        {children}
      </TouchableOpacity>
    )
  }
  return <View style={{ ...styles.card, ...style }}>{children}</View>
}

Card.propTypes = {
  onPress: PropTypes.func,
  style: PropTypes.object
}

Card.defaultProps = {
  onPress: null,
  style: null
}

export default Card

const styles = StyleSheet.create({
  card: {
    backgroundColor: colors.backgroundCard,
    marginBottom: 10,
    padding: 10,
    borderRadius: 5,
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 7
  }
})
