import React, { Component } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Creators } from '~/store/ducks/message'
import colors from '~/styles/colors'

class MessageBar extends Component {
  render() {
    const { message, removeMessage } = this.props
    const messageView = (
      <View style={{ ...styles.container }}>
        <Text style={{ color: 'yellow' }}>{message || null}</Text>
      </View>
    )

    if (message) {
      setTimeout(() => {
        removeMessage()
      }, 3000)
      return messageView
    }

    return null
  }
}

export const validationMessageSubmit = (errors, showMessage, handleSubmit) => {
  if (errors && Object.values(errors)) {
    Object.values(errors).forEach((value) => {
      showMessage({ type: 'error', message: value })
    })
  }
  handleSubmit()
}

const mapStateToProps = state => ({
  message: state.message.message
})

const mapDispatchToProps = dispatch => bindActionCreators(
  { removeMessage: Creators.removeMessage, showMessage: Creators.showMessage },
  dispatch
)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MessageBar)

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: colors.blackTransparent,
    padding: 10,
    zIndex: 9999
  }
})
