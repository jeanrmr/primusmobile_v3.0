import React from 'react'
import { TouchableOpacity } from 'react-native'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Icon } from 'native-base'
import styles from './styles'

const newItemButton = props => (
  <TouchableOpacity transparent onPress={props.onPress} style={{ marginRight: 10 }}>
    <Icon name="add" style={styles.icon} />
  </TouchableOpacity>
)

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(newItemButton)
