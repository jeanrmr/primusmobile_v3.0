import React from 'react'
import { shallow } from 'enzyme'
import { newItemButton } from './newItemButton'

describe('render', () => {
  const wrapper = shallow(<newItemButton />)
  it('match snap', () => {
    expect(wrapper).toMatchSnapshot()
  })
})
