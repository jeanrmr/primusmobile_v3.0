import React from 'react'
import {
  View,
  TouchableOpacity,
  Text,
  Image,
  PixelRatio,
  Alert,
  StyleSheet,
  Platform
} from 'react-native'
import { Icon } from 'native-base'
import LinearGradient from 'react-native-linear-gradient'
import { ifIphoneX } from 'react-native-iphone-x-helper'
import PropTypes from 'prop-types'
import colors from '~/styles/colors'
import { logout } from '~/store/ducks/auth'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

const homeHeaderImage = require('~/../assets/homeHeader.jpg')

const size = PixelRatio.getPixelSizeForLayoutSize

export class Header extends React.Component {
  static propTypes = {
    title: PropTypes.string,
    marginBottom: PropTypes.number,
    back: PropTypes.bool,
    hideOut: PropTypes.bool
  }

  static defaultProps = {
    title: '',
    marginBottom: 220,
    back: false,
    hideOut: false
  }

  blur = Platform.OS === 'ios' ? 5.0 : 2.0

  getMarginBottom = () => {
    const { marginBottom, children } = this.props
    if (marginBottom !== undefined) {
      return marginBottom
    }
    if (children !== undefined) {
      return 30
    }
    return marginBottom
  }

  linearBackground = () => {
    const { children } = this.props
    if (!children) {
      return (
        <View>
          <LinearGradient
            colors={['rgba(0,0,0, 1)', 'rgba(0,0,0, 0.0)']}
            style={{
              position: 'absolute',
              top: 0,
              left: 0,
              width: '100%',
              ...ifIphoneX({ height: 120 }, { height: 60 })
            }}
          />

          <LinearGradient
            colors={['rgba(0,0,0, 0.5)', 'rgba(0,0,0, 0.0)']}
            style={{
              position: 'absolute',
              top: 0,
              left: 0,
              width: '100%',
              ...ifIphoneX({ height: 120 }, { height: 60 })
            }}
          />
        </View>
      )
    }
    return (
      <LinearGradient
        colors={['rgba(0,0,0, 0.4)', 'rgba(0,0,0, 0.4)']}
        style={{
          position: 'absolute',
          top: 0,
          left: 0,
          width: '100%',
          ...ifIphoneX({ height: 230 }, { height: 200 })
        }}
      />
    )
  }

  showConfirmDialog = () => {
    const { navigation, logoutThunk } = this.props
    Alert.alert(
      '',
      'Deseja realmente sair?',
      [
        { text: 'Sair', onPress: () => logoutThunk(navigation) },
        { text: 'Cancel', style: 'cancel' }
      ],
      { cancelable: true }
    )
  }

  render() {
    const {
      children, title, back, onGoBack, navigation, hideOut
    } = this.props
    return (
      <View style={{ marginBottom: this.getMarginBottom() }}>
        <View style={styles.backgroundImageContainer}>
          <Image
            blurRadius={children ? this.blur : 0}
            style={styles.backgroundImage}
            source={homeHeaderImage}
          />
        </View>

        {this.linearBackground()}

        <LinearGradient
          colors={['rgba(32,45,57, 0)', 'rgba(32,45,57, 1)']}
          style={styles.linearGradient}
        />

        <View style={styles.contentContainer}>
          <Text style={styles.title}>{title}</Text>

          {back ? (
            <TouchableOpacity
              transparent
              style={{ alignSelf: 'flex-start', position: 'absolute' }}
              hitSlop={{
                top: 10,
                bottom: 10,
                left: 10,
                right: 10
              }}
              onPress={() => {
                if (onGoBack) onGoBack()
                navigation.goBack()
              }}
            >
              <Icon name="arrow-round-back" style={styles.backIcon} />
            </TouchableOpacity>
          ) : null}

          {hideOut ? null : (
            <TouchableOpacity transparent style={{ alignSelf: 'flex-end', position: 'absolute' }}>
              <Icon
                name="log-out"
                style={{ color: colors.defaultText, marginRight: size(3), marginTop: size(2) }}
                onPress={() => this.showConfirmDialog()}
                hitSlop={{
                  top: 10,
                  bottom: 10,
                  left: 10,
                  right: 10
                }}
              />
            </TouchableOpacity>
          )}

          {children}
        </View>
      </View>
    )
  }
}

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    logoutThunk: logout
  },
  dispatch
)

export default connect(
  null,
  mapDispatchToProps
)(Header)

const styles = StyleSheet.create({
  contentContainer: {
    ...ifIphoneX({ marginTop: 30 })
  },
  title: {
    color: colors.defaultText,
    fontWeight: 'bold',
    fontSize: 18,
    alignSelf: 'center',
    position: 'absolute',
    marginTop: size(5)
  },
  backgroundImageContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    overflow: 'hidden',
    ...ifIphoneX({ height: 230 }, { height: 200 })
  },
  backgroundImage: {
    ...ifIphoneX({ height: 230 }, { height: 200 }),
    width: '100%'
  },
  linearGradient: {
    position: 'absolute',
    ...ifIphoneX({ top: 190 }, { top: 160 }),
    left: 0,
    width: '100%',
    height: 40
  },
  backIcon: {
    fontSize: 38,
    color: colors.defaultText,
    marginLeft: size(3),
    marginTop: size(1)
  }
})
