import React from 'react'
import { shallow } from 'enzyme'
import { backButton } from './backButton'

describe('render', () => {
  const wrapper = shallow(<backButton />)
  it('match snap', () => {
    expect(wrapper).toMatchSnapshot()
  })
})
