import { StyleSheet } from 'react-native'
import colors from '~/styles/colors'

export default StyleSheet.create({
  container: { position: 'absolute', top: 0 },
  backIcon: {
    fontSize: 38,
    color: colors.defaultText,
    marginLeft: 10,
    marginTop: 5
  }
})
