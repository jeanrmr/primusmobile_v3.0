import React from 'react'
import { TouchableOpacity, View } from 'react-native'
import NavigationService from '~/navigation/NavigationService'
import { Icon } from 'native-base'
import PropTypes from 'prop-types'
import styles from './styles'

const BackButton = props => (
  <View style={styles.container}>
    <TouchableOpacity
      transparent
      hitSlop={{
        top: 10,
        bottom: 10,
        left: 10,
        right: 10
      }}
      onPress={() => {
        if (props.onGoBack) props.onGoBack()
        NavigationService.goBack()
      }}
    >
      <Icon name="arrow-round-back" style={styles.backIcon} />
    </TouchableOpacity>
  </View>
)

BackButton.propTypes = {
  onGoBack: PropTypes.func
}

BackButton.defaultProps = {
  onGoBack: null
}

export default BackButton
