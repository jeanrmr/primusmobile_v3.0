import { combineReducers } from 'redux'
import AuthReducer from './auth'
import MessageReducer from './message'
import serviceReducer from './service'
import errorReducer from './error'

export const rootReducer = combineReducers({
  error: errorReducer,
  credentials: AuthReducer,
  message: MessageReducer,
  service: serviceReducer
})
