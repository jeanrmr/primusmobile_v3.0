import AsyncStorage from '@react-native-community/async-storage'
import Axios from 'axios'
import { createActions, createReducer } from 'reduxsauce'
import { Creators as loader } from '../loader'
import { Creators as message } from '../message'

export const { Types, Creators } = createActions({
  asyncSignIn: ['user'],
  saveUser: ['value'],
  doneSignIn: ['value'],
  removeUser: []
})

export function verifyUserInfo(navigation) {
  return async (dispatch) => {
    try {
      let user = await AsyncStorage.getItem('user')
      if (user) {
        user = JSON.parse(user)
        Axios.defaults.headers.Authorization = `bearer ${user.token}`
        dispatch(Creators.saveUser(user))
      }
      navigation.navigate(user ? 'app' : 'auth')
      dispatch(loader.disable())
    } catch (err) {
      dispatch(message.showMessage({ message: err.message }))
    }
  }
}

export const logout = navigation => async (dispatch) => {
  try {
    dispatch(loader.enable())
    await AsyncStorage.removeItem('user')
    Axios.defaults.headers.Authorization = undefined
    dispatch(Creators.removeUser())
    dispatch(loader.disable())
    navigation.navigate('auth')
  } catch (err) {
    dispatch(loader.disable())
    dispatch(message.showMessage({ message: err.message }))
  }
}

const INITIAL_STATE = { verified: false, user: null }

export default createReducer(INITIAL_STATE, {
  [Types.DONE_SIGN_IN]: saveReducer,
  [Types.REMOVE_USER]: removeReducer
})

const saveReducer = (state = INITIAL_STATE, value) => ({
  ...state,
  user: value,
  verified: true
})

const removeReducer = (state = INITIAL_STATE) => ({
  ...state,
  user: undefined,
  verified: false
})
