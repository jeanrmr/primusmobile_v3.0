import configureStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import promise from 'redux-promise'
import AsyncStorage from '@react-native-community/async-storage'
import { Types, Creators as auth, verifyUserInfo, logout } from './index'
import { Creators as loader } from '../loader'
import Axios from 'axios'

describe('test Types', () => {
  it('REMOVE_USER', () => {
    expect(auth.removeUser()).toEqual({ type: 'REMOVE_USER' })
  })

  it('SAVE_USER', () => {
    expect(auth.saveUser()).toEqual({ type: 'SAVE_USER' })
  })
})

describe('Test Actions', () => {
  initialState = {}
  createStore = configureStore([thunk, promise])

  it('verifyUserInfo without user loged', async () => {
    let store = createStore(initialState)
    let mockNavigation = { navigate: jest.fn() }
    func = verifyUserInfo(mockNavigation)
    await func(store.dispatch)
    expect(mockNavigation.navigate.mock.calls.length).toBe(1)
    expect(mockNavigation.navigate.mock.calls[0][0]).toBe('auth')
    expect(store.getActions()).toEqual([loader.disable()])
  })

  it('verifyUserInfo with user loged', async () => {
    let store = createStore(initialState)
    let navigate = jest.fn()
    let mockNavigation = { navigate }
    user = { token: '1234' }
    await AsyncStorage.setItem('user', JSON.stringify(user))
    let func = verifyUserInfo(mockNavigation)
    await func(store.dispatch)
    expect(store.getActions()).toEqual([auth.saveUser(user), loader.disable()])
    expect(mockNavigation.navigate.mock.calls.length).toBe(1)
    expect(mockNavigation.navigate.mock.calls[0][0]).toBe('app')
    expect(Axios.defaults.headers.Authorization).toBe('bearer 1234')
    expect(navigate.mock.calls[0][0]).toBe('app')
    expect(await AsyncStorage.getItem('user')).toBe('{"token":"1234"}')
  })

  it('removeUser', async () => {
    Axios.defaults.headers.Authorization = 'userExample'
    await AsyncStorage.setItem('user', 'userExample')
    let store = createStore(initialState)
    let navigate = jest.fn()
    let mockNavigation = { navigate }
    let func = logout(mockNavigation)
    await func(store.dispatch)
    expect(store.getActions()).toEqual([loader.enable(), auth.removeUser(), loader.disable()])
    expect(Axios.defaults.headers.Authorization).toBe(undefined)
    expect(navigate.mock.calls.length).toBe(1)
    expect(navigate.mock.calls[0][0]).toBe('auth')
    expect(await AsyncStorage.getItem('user')).toBe(null)
  })
})
