import { createActions, createReducer } from 'reduxsauce'

export const { Types, Creators } = createActions({
  showError: []
})

const INITIAL_STATE = { error: false }

const showError = (state = INITIAL_STATE) => ({
  ...state,
  error: true
})

export default createReducer(INITIAL_STATE, {
  [Types.SHOW_ERROR]: showError
})
