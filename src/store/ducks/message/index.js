import { createActions, createReducer } from 'reduxsauce'

export const { Types, Creators } = createActions({
  showMessage: ['message'],
  removeMessage: []
})

const INITIAL_STATE = { type: null, message: null }

const showMessage = (state = INITIAL_STATE, payload) => ({
  ...state,
  type: payload.message.type,
  message: payload.message.message
})

const removeMessage = (state = INITIAL_STATE) => ({
  ...state,
  type: null,
  message: null
})

export default createReducer(INITIAL_STATE, {
  [Types.SHOW_MESSAGE]: showMessage,
  [Types.REMOVE_MESSAGE]: removeMessage
})
