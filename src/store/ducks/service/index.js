import { createActions, createReducer } from 'reduxsauce'

export const { Types, Creators } = createActions({
  asyncFindAllServices: [],
  asyncSaveService: ['service'],
  asyncDeleteService: ['id'],
  editService: ['service'],
  cleanEditService: [],
  doneSaveService: [],
  doneFindAllServices: ['services'],
  doneDeleteService: []
})

const INITIAL_STATE = {
  services: [],
  editService: {}
}

const doneFindAllServices = (state = INITIAL_STATE, payload) => ({
  ...state,
  services: payload.services
})

const editService = (state = INITIAL_STATE, payload) => ({
  ...state,
  editService: { ...payload.service }
})

const clean = (state = INITIAL_STATE) => ({
  ...state,
  editService: {}
})

export default createReducer(INITIAL_STATE, {
  [Types.EDIT_SERVICE]: editService,
  [Types.CLEAN_EDIT_SERVICE]: clean,
  [Types.DONE_FIND_ALL_SERVICES]: doneFindAllServices
})
