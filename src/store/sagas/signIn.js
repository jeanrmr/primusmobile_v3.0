import { takeLatest, put, call } from 'redux-saga/effects'
import AsyncStorage from '@react-native-community/async-storage'
import Axios from 'axios'
import { Creators as auth } from '~/store/ducks/auth'
import NavigationService from '~/navigation/NavigationService'
import safeWrapper from './safeWrapper'

function* signIn({ user }) {
  const oneSignalId = yield call(AsyncStorage.getItem, 'oneSignalId')
  const response = yield call(Axios.post, 'session', { ...user, oneSignalId })
  yield call(AsyncStorage.setItem, 'user', JSON.stringify(response.data))
  yield put(auth.saveUser(response.data))
  Axios.defaults.headers.Authorization = `bearer ${response.token}`
  yield put(auth.doneSignIn(response.data))
  NavigationService.navigate('app')
}

export function* watchSignIn() {
  yield takeLatest('ASYNC_SIGN_IN', safeWrapper(signIn))
}

export default [watchSignIn()]
