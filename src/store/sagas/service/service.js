import { takeLatest, put, call } from 'redux-saga/effects'
import Axios from 'axios'
import { Creators as serviceCreators } from '~/store/ducks/service'
import { showMessage } from 'react-native-flash-message'
import reactotron from 'reactotron-react-native'
import safeWrapper from '../safeWrapper'
import NavigationService from '~/navigation/NavigationService'

function* findAll() {
  const response = yield call(Axios.get, 'services')
  yield put(serviceCreators.doneFindAllServices(response.data))
}

function* watchFindAll() {
  yield takeLatest('ASYNC_FIND_ALL_SERVICES', safeWrapper(findAll))
}

function* save(action) {
  const { service } = action
  reactotron.log(action.service)
  const res = service.id
    ? yield call(Axios.put, `services/${service.id}`, service)
    : yield call(Axios.post, 'services', service)
  let messageObj
  if (res.status === 200) {
    messageObj = service.id
      ? { type: 'success', message: 'Serviço atualizado' }
      : { type: 'success', message: 'Serviço cadastrado' }
  }
  showMessage(messageObj)
  yield put(serviceCreators.asyncFindAllServices())
  NavigationService.goBack()
}

function* watchSave() {
  yield takeLatest('ASYNC_SAVE_SERVICE', safeWrapper(save))
}

function* deleteService(action) {
  const { id } = action
  const res = yield call(Axios.delete, `services/${id}`)
  if (res.status === 200) showMessage({ type: 'success', message: 'Serviço excluído' })
  yield put(serviceCreators.asyncFindAllServices())
  NavigationService.goBack()
}

function* watchDelete() {
  yield takeLatest('ASYNC_DELETE_SERVICE', safeWrapper(deleteService))
}

export default [watchFindAll(), watchSave(), watchDelete()]
