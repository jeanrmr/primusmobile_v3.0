import { take } from 'redux-saga/effects'
import { enable as enableLoader, disable as disableLoader } from '~/common/components/Loader'

export function* watchAsync() {
  while (true) {
    const action = yield take('*')
    if (action.type.match('ASYNC*')) {
      enableLoader()
    } else if (action.type.match('DONE*') || action.type.match('ERROR*')) {
      disableLoader()
    }
  }
}

export default [watchAsync()]
