/* eslint-disable func-names */
/* eslint-disable consistent-return */
import { put } from 'redux-saga/effects'
import { showMessage } from 'react-native-flash-message'

export default function (effect) {
  return function* (action) {
    try {
      yield effect(action)
    } catch (error) {
      showMessage({ type: 'danger', message: error.message })
      yield put({ type: 'ERROR', payload: error })
    }
  }
}
