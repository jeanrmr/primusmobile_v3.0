import { all } from 'redux-saga/effects'
import loading from './loading'
import signIn from './signIn'
import service from './service'

const sagas = [...loading, ...signIn, ...service]

export default function* rootSaga() {
  yield all(sagas)
}
