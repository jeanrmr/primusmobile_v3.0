import thunk from 'redux-thunk'
import { createStore, applyMiddleware, compose } from 'redux'
import createSagaMiddleware from 'redux-saga'
import Reactotron from 'reactotron-react-native'
import { rootReducer } from './ducks'
import ReactotronConfig from '../../ReactotronConfig'
import rootSaga from '~/store/sagas'

const sagaMonitor = Reactotron.createSagaMonitor()
const sagaMiddleware = createSagaMiddleware({ sagaMonitor })

const middlewares = [thunk, sagaMiddleware]

const composer = __DEV__
  ? compose(
    applyMiddleware(...middlewares),
    ReactotronConfig.createEnhancer()
  )
  : compose(applyMiddleware(...middlewares))

const store = createStore(rootReducer, composer)

sagaMiddleware.run(rootSaga)

export default store
