import { createStackNavigator } from 'react-navigation'
import ProfessionalList from '../screens/Professionals'
import BarberInfoScreen from '../barber/BarberInfoScreen'

export default stack = createStackNavigator(
  {
    barberList: ProfessionalList,
    barberEdit: BarberInfoScreen
  },
  {
    gesturesEnabled: true,
    initialRouteName: 'barberList',
    mode: 'modal',
    headerTransitionPreset: 'fade-in-place'
  }
)
