import { createStackNavigator } from 'react-navigation'
import SignInScreen from '~/screens/signIn'
import SignUpScreen from '~/screens/signup'

const authStackNav = createStackNavigator(
  {
    signIn: SignInScreen,
    signUp: {
      screen: SignUpScreen,
      navigationOptions: () => ({
        header: null
      })
    }
  },
  {
    gesturesEnabled: true,
    initialRouteName: 'signIn',
    mode: 'modal',
    headerTransitionPreset: 'fade-in-place'
  }
)
export default authStackNav
