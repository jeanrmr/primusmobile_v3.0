import { createStackNavigator } from 'react-navigation'
import Profile from '../screens/Profile'
import EditProfile from '~/screens/EditProfile'
import ProfessionalsStack from '~/navigation/ProfessionalsStack'
import ServiceStack from './ServiceStack'
import SettingsStack from '~/navigation/settingsStack'

export default createStackNavigator({
  profile: {
    screen: Profile,
    navigationOptions: () => ({
      header: null
    })
  },
  editProfile: {
    screen: EditProfile,
    navigationOptions: () => ({
      header: null
    })
  },
  professionals: {
    screen: ProfessionalsStack,
    navigationOptions: () => ({
      header: null
    })
  },
  services: {
    screen: ServiceStack,
    navigationOptions: () => ({
      header: null
    })
  },
  settings: {
    screen: SettingsStack,
    navigationOptions: () => ({
      header: null
    })
  }
})
