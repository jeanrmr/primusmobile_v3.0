import { createMaterialTopTabNavigator } from 'react-navigation'
import selectService from '../screens/SelectServices'
import selectBarber from '../screens/SelectBarber'
import selectTime from '../screens/SelectTime'
import colors from '../styles/colors'

export default createMaterialTopTabNavigator(
  {
    service: {
      screen: selectService,
      navigationOptions: () => ({
        tabBarLabel: 'Serviços'
      })
    },
    barber: {
      screen: selectBarber,
      navigationOptions: () => ({
        tabBarLabel: 'Profissionais'
      })
    },
    time: {
      screen: selectTime,
      navigationOptions: () => ({
        tabBarLabel: 'Horário'
      })
    }
  },
  {
    initialRouteName: 'service',
    swipeEnabled: false,
    tabBarOptions: {
      style: {
        backgroundColor: colors.highBackground
      },
      indicatorStyle: {
        backgroundColor: colors.hightLightText
      },
      labelStyle: {
        color: colors.defaultText,
        fontSize: 11
      },
      activeTintColor: colors.hightLightText
    }
  }
)
