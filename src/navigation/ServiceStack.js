import { createStackNavigator } from 'react-navigation'
import Service from '~/screens/service'
import EditService from '~/screens/EditService'

export default createStackNavigator({
  serviceList: {
    screen: Service
  },
  editService: {
    screen: EditService
  }
}, {
  initialRouteName: 'serviceList',
  headerTransitionPreset: 'fade-in-place'
})
