import { createStackNavigator } from 'react-navigation'
import ProfessionalList from '~/screens/Professionals'
import { EditProfessional as edit } from '~/screens/EditProfessional'


export default createStackNavigator({
  professionals: {
    screen: ProfessionalList
  },
  editProfessionalInfo: {
    screen: edit,
    navigationOptions: props => ({
      header: null
    })
  }
},
{
  initialRouteName: 'professionals',
  headerTransitionPreset: 'fade-in-place'
})
