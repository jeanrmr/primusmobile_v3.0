import React from 'react'
import { createAppContainer } from 'react-navigation'
import AuthStack from '~/navigation/authStack'
import AppBottomNavigation from '~/navigation/AppBottomTopTab'
import createAnimatedSwitchNavigator from 'react-navigation-animated-switch'

import AuthLoadingScreen from '~/screens/AuthLoading'
import { Transition } from 'react-native-reanimated'
ENV = 'development'

const initialRouteName = ENV === 'development' ? 'app' : 'loading'
// const initialRouteName = 'loading'

const switchNavigation = createAnimatedSwitchNavigator(
  {
    loading: AuthLoadingScreen,
    app: AppBottomNavigation,
    auth: AuthStack
  },
  {
    transition: (
      <Transition.Together>
        <Transition.In type="fade" durationMs={500} interpolation="easeInOut" />
        <Transition.Out type="slide-bottom" durationMs={500} interpolation="easeInOut" />
      </Transition.Together>
    ),
    initialRouteName
  }
)

export default createAppContainer(switchNavigation)
