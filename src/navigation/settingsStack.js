import { createStackNavigator } from 'react-navigation'
import Settings from '~/screens/settings'
import TimeBetweenAppoitments from '~/screens/timeBetweenAppointments'

export default createStackNavigator(
  {
    settings: Settings,
    timeBetweenAppoitments: TimeBetweenAppoitments
  },
  {
    gesturesEnabled: true,
    initialRouteName: 'settings',
    mode: 'card',
    headerTransitionPreset: 'fade-in-place'
  }
)
