module.exports = {
  extends: 'airbnb',
  parser: 'babel-eslint',
  env: {
    jest: true
  },
  rules: {
    'no-use-before-define': 'off',
    'react/jsx-filename-extension': 'off',
    'react/prop-types': 'off',
    'comma-dangle': 'off',
    semi: ['error', 'never'],
    'import/prefer-default-export': 'off',
    'react/jsx-indent': 'space',
    'react/jsx-indent-props': 'space',
    'global-require': 'off',
    'no-plusplus': ['error', { allowForLoopAfterthoughts: true }]
  },
  globals: {
    fetch: false,
    __DEV__: false
  },
  settings: {
    'import/resolver': {
      'babel-plugin-root-import': {
        rootPathPrefix: '~',
        rootPathSuffix: 'src'
      }
    }
  }
}
