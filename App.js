/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react'
// import SplashScreen from 'react-native-splash-screen'
import { Provider } from 'react-redux'
import { StatusBar } from 'react-native'
import FlashMessage from 'react-native-flash-message'
import Navigation from './src/navigation'
import store from './src/store'
import httpConfig from '~/config/http'
import NavigationService from './src/navigation/NavigationService'
import Loader from './src/common/components/Loader'
import MessageBar from '~/common/components/MessageBar'
import notification from '~/config/notification'
import ErrorBondary from '~/screens/error'
import { setRef } from '~/common/components/Loader/loaderService'
import { ANOTHER_CONFIG } from 'react-native-dotenv'
import reactotron from 'reactotron-react-native'

export default class App extends Component {
  componentWillMount() {
    console.disableYellowBox = true
  }

  componentDidMount() {
    reactotron.log(ANOTHER_CONFIG)
    // SplashScreen.hide()

    notification.init()
    httpConfig(store)
  }

  componentWillUnmount() {
    notification.removeListeners()
  }

  render() {
    return (
      <Provider store={store}>
        <ErrorBondary>
          <StatusBar hidden />
          <Loader ref={ref => setRef(ref)} />
          <MessageBar />
          <Navigation ref={navigatorRef => NavigationService.setTopLevelNavigator(navigatorRef)} />
          <FlashMessage position="bottom" />
        </ErrorBondary>
      </Provider>
    )
  }
}
