// eslint-disable-next-line import/no-extraneous-dependencies
import Reactotron from 'reactotron-react-native'
// eslint-disable-next-line import/no-extraneous-dependencies
import { reactotronRedux } from 'reactotron-redux'
import sagaPlugin from 'reactotron-redux-saga'

// then add it to the plugin list

const reactotron = Reactotron.configure({ host: '10.0.0.122', name: 'Primus' })
  .useReactNative()
  .use(reactotronRedux())
  .use(sagaPlugin())
  .connect()

export default reactotron
