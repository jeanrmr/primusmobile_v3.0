import {
  Generator, File, casex, namedCasex
} from 'battlecry'

const SCREENS_PATH = 'src/screens'

export default class ClassScreenGenerator extends Generator {
  config = {
    generate: {
      args: 'name',
      options: {
        connected: { description: 'is the component connect to redux?' }
      }
    }
  }

  get folder() {
    return `src/screens/__naMe__/`
  }

  get componentFile() {
    return `${SCREENS_PATH}/${this.args.name}/${this.args.name}.js`
  }

  resolveImportsExports(file) {
    if (this.options.connected) {
      file
        .afterLast('import { Text, View }', "import { bindActionCreators } from 'redux'")
        .afterLast('import ', "import { connect } from 'react-redux'")
        .replaceText('export default class', 'export class')
    }
    return file
  }

  revolveReduxStuffs(file) {
    file.append(
      `
const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(${casex(this.args.name, 'CaSe')})
`
    )
    return file
  }

  generate() {
    this.templates().forEach(file => file.saveAs(this.folder, this.args.name))
    if (this.options.connected) {
      const file = new File(this.componentFile)
      this.resolveImportsExports(file)
      this.revolveReduxStuffs(file)
      file.save()
    }
  }
}
