import { createActions, createReducer } from 'reduxsauce'

export const { Types, Creators } = createActions({})

const INITIAL_STATE = {}

export default createReducer(INITIAL_STATE, {})
