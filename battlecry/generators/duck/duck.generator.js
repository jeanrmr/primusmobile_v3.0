import { Generator, File, casex } from 'battlecry'

const DUCKS_PATH = 'src/store/ducks'
const ROOT_REDUCER = 'src/store/ducks/index.js'

export default class DuckGenerator extends Generator {
  config = {
    generate: {
      args: 'name',
      options: {
        special: { description: 'Special option' }
      }
    }
  }

  get reducer() {
    return `  ${this.args.name}: ${casex(this.args.name, 'caSe')}Reducer,`
  }

  generate() {
    this.templates().forEach(file => file.saveAs(`${DUCKS_PATH}/__naMe__/`, this.args.name))
    new File(ROOT_REDUCER)
      .afterLast(
        'import',
        `import ${casex(this.args.name, 'caSe')}Reducer from './${casex(this.args.name, 'caSe')}'`
      )
      .after('export const rootReducer = combineReducers({', this.reducer)
      .save()
  }
}
