import React from 'react'
import { shallow } from 'enzyme'
import { __NaMe__ } from './__NaMe__'

describe('render', () => {
  const wrapper = shallow(<__NaMe__ />)
  it('match snap', () => {
    expect(wrapper).toMatchSnapshot()
  })
})
