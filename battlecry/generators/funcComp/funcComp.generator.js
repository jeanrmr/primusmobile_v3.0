import { Generator, File, casex } from 'battlecry'

const COMP_PATH = 'src/common/components'

export default class FuncCompGenerator extends Generator {
  config = {
    generate: {
      args: 'name',
      options: {
        connected: { description: 'is the component connect to redux?' }
      }
    }
  }

  resolveImportsExports(file) {
    if (this.options.connected) {
      file
        .afterLast('import { Text, View }', "import { bindActionCreators } from 'redux'")
        .afterLast('import ', "import { connect } from 'react-redux'")
    }
    return file
  }

  revolveReduxStuffs(file) {
    file.append(
      `const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(${casex(this.args.name, 'caSe')})
`
    )
    return file
  }

  removePreviosExportDefault(file) {
    file.remove('export default')
  }

  get folder() {
    return `src/common/components/__naMe__/`
  }

  get componentFile() {
    return `${COMP_PATH}/${this.args.name}/${this.args.name}.js`
  }

  generate() {
    this.templates().forEach(file => file.saveAs(this.folder, this.args.name))
    if (this.options.connected) {
      const file = new File(this.componentFile)
      this.resolveImportsExports(file)
      this.removePreviosExportDefault(file)
      this.revolveReduxStuffs(file)
      file.save()
    }
  }
}
