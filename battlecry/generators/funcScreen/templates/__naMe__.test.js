import React from 'react'
import { shallow } from 'enzyme'
import { __naMe__ } from './__naMe__'

describe('render', () => {
  const wrapper = shallow(<__naMe__ />)
  it('match snap', () => {
    expect(wrapper).toMatchSnapshot()
  })
})
